# Paging Mission Control

Finds satellites that are in Alert state and returns all records meeting alert state for the satellite. Below are the Alert state conditions
* If for the same satellite there are three thermostat readings that exceed the red high limit within a five minute interval.
* If for the same satellite there are three battery voltage readings that are under the red low limit within a five minute interval.


## Commands
```
 Usage: pmc [OPTIONS]

 Parse telemetry data .

╭─ Options ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ *  --data-file                 TEXT                             The telemetry data file [default: None] [required]                         │
│    --install-completion        [bash|zsh|fish|powershell|pwsh]  Install completion for the specified shell. [default: None]                │
│    --show-completion           [bash|zsh|fish|powershell|pwsh]  Show completion for the specified shell, to copy it or customize the       │
│                                                                 installation.                                                              │
│                                                                 [default: None]                                                            │
│    --help                                                       Show this message and exit.                                                │
╰────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
```
