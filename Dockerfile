ARG IMAGE="python"
ARG TAG="3.10.3-slim-buster"

FROM ${IMAGE}:${TAG} as python-base
ENV PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    POETRY_NO_INTERACTION=1 \
    POETRY_VERSION='1.1.13' \
    POETRY_HOME="/opt/poetry" \
    POETRY_APP_WORKDIR="/opt/app" \
    VENV_PATH="${POETRY_APP_WORKDIR}/.venv"

ENV PATH="$POETRY_HOME/bin:$VENV_PATH/bin:$PATH"

FROM python-base as builder-base
RUN apt-get update \
    && apt-get install --no-install-recommends -y \
        curl \
        build-essential
RUN curl -sSL https://install.python-poetry.org | python -
WORKDIR ${POETRY_APP_WORKDIR}
COPY poetry.lock pyproject.toml ./
RUN poetry install --no-interaction --no-ansi --no-dev

FROM builder-base as qa-runner
WORKDIR ${POETRY_APP_WORKDIR}
COPY --from=builder-base $POETRY_HOME POETRY_HOME
COPY --from=builder-base $POETRY_APP_WORKDIR $POETRY_APP_WORKDIR
RUN poetry install --no-interaction --no-ansi
COPY . ${POETRY_APP_WORKDIR}/
RUN poetry run mkdocs build -q && \
    poetry run pydocstyle . && \
    poetry run flake8 --config .flake8 && \
    poetry run bandit -q -r . -c .bandit.yml && \
    poetry run safety check --json --output safety.json
