import unittest

from pmc import satellite_alert


class TestSatelliteData(unittest.TestCase):
    def test_parse_line(self):
        line = "20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT"
        (
            time_stamp,
            satellite_id,
            red_high,
            yellow_high,
            yellow_low,
            red_low,
            raw_val,
            component,
        ) = satellite_alert.parse_log_line(line)

        self.assertEqual(time_stamp, "20180101 23:01:05.001")
        self.assertEqual(satellite_id, "1001")
        self.assertEqual(red_high, "101")
        self.assertEqual(yellow_high, "98")
        self.assertEqual(yellow_low, "25")
        self.assertEqual(red_low, "20")
        self.assertEqual(raw_val, "99.9")
        self.assertEqual(component, "TSTAT")

    def test_threshold(self):
        high_tstat_values = {
            "raw_value": "999.9",
            "red_high": "101",
            "red_low": "20",
            "component": "TSTAT",
        }

        low_tstat_values = {
            "raw_value": "9.9",
            "red_high": "101",
            "red_low": "20",
            "component": "TSTAT",
        }

        self.assertEqual(satellite_alert.check_threshold(**high_tstat_values), True)
        self.assertEqual(satellite_alert.check_threshold(**low_tstat_values), False)
