# -*- coding: utf-8 -*-
"""PMC Module."""

import typer

from pmc.satellite_alert import run

app = typer.Typer()


@app.command("telemetry-data")
def telemetry_data(data_file: str = typer.Option(..., help="The telemetry data file")):
    """Parse telemetry data ."""
    msg = run(data_file)
    typer.echo(f"{msg}")
