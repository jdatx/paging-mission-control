import logging
import os

from datetime import datetime, timedelta

from typing import Dict, List, Tuple

log_file = os.getenv("SATELLITE_LOG_FILE")

logging.basicConfig(
    filename=log_file,
    level=logging.DEBUG,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
)


def check_threshold(
    raw_value: str, red_high: str, red_low: str, component: str
) -> bool:
    """Check the raw value for threshold for component type.

    Args:
        raw_value (str): the raw value for the record
        red_high (str): the red high value for the record
        red_low (str): the red low value for the record
        component (str): the component type

    Returns:
        bool: true if component recored hits threshold

    """
    result = False
    raw = float(raw_value)

    if component == "TSTAT":
        result = raw > int(red_high)
    if component == "BATT":
        result = raw < int(red_low)

    return result


def parse_log_line(line: str) -> Tuple:
    """Parse the line in the satellite logs

    Args:
        line (str): the line in the logs file

    Returns:
        Tuple of results
    """
    (
        time_stamp,
        satellite_id,
        red_high,
        yellow_high,
        yellow_low,
        red_low,
        raw_val,
        component,
    ) = map(lambda s: s.strip(), line.strip().split("|"))

    return (
        time_stamp,
        satellite_id,
        red_high,
        yellow_high,
        yellow_low,
        red_low,
        raw_val,
        component,
    )


def get_threshold_hits(data) -> List:
    """Checks the logs records thresholds

    Args:
        data: satellite log file

    Returns:
        List of satellites in alarm state
    """

    satellite_data = []

    with open(data, "r") as fp:
        lines = fp.readlines()

    for line in lines:
        (
            time_stamp,
            satellite_id,
            red_high,
            yellow_high,
            yellow_low,
            red_low,
            raw_val,
            component,
        ) = parse_log_line(line=line)

        threshold_hit = check_threshold(raw_val, red_high, red_low, component)

        if threshold_hit:
            if component == "TSTAT":
                severity = "RED HIGH"
            if component == "BATT":
                severity = "RED LOW"

            satellite_data.append(
                {
                    "satelliteId": satellite_id,
                    "component": component,
                    "timestamp": time_stamp,
                    "severity": severity,
                }
            )
    return satellite_data


def set_alert_component(alert_id: List, threshold_hits: List) -> Dict:
    """Set alert component for satellite ids.

    Args:
        alert_id (List): alert ids to set component data for
        threshold_hits (List):

    Returns:
        Dict of satellites with threshold alerts per component.
    """
    alert_data = {
        k: {
            "data": [s for s in threshold_hits if s["satelliteId"] == k],
            "BATT": None,
            "TSTAT": None,
        }
        for k in alert_id
    }

    for sth in alert_id:
        alert_data[sth]["BATT"] = [
            s["timestamp"]
            for s in threshold_hits
            if s["satelliteId"] == sth
            if s["component"] == "BATT"
        ]
        alert_data[sth]["TSTAT"] = [
            s["timestamp"]
            for s in threshold_hits
            if s["satelliteId"] == sth
            if s["component"] == "TSTAT"
        ]

    return alert_data


def set_component_time_delta(component_alerts: List) -> None:
    """Set component time delta.

    Args:
        component_alerts (List): list of component alerts

    Returns:
        bool: true if component alerts meet time threshold
    """
    sorted_components = sorted(component_alerts)
    first_alert = datetime.strptime(sorted_components[0], "%Y%m%d %H:%M:%S.%f")
    last_alert = datetime.strptime(sorted_components[-1], "%Y%m%d %H:%M:%S.%f")

    if abs(last_alert - first_alert):
        return True
    return False


def set_alert_notif(hit_ids: List, alert_data: Dict) -> List:
    """Set alert notification

    Args:
        hit_ids (List): satellite ids that have threshold hit
        alert_data (Dict): satelite data

    Returns:
        List of satelites with alert data
    """
    alert_notif = []

    for sate_id in hit_ids:
        batt_alerts = alert_data[sate_id]["BATT"]
        thermo_alerts = alert_data[sate_id]["TSTAT"]

        for component_alert in [batt_alerts, thermo_alerts]:
            if len(component_alert) >= 3:
                if set_component_time_delta(component_alert):
                    for d in alert_data[sate_id]["data"]:
                        if d["timestamp"] in component_alert:
                            alert_notif.append(d)
    return alert_notif


def run(data) -> None:
    """Run the sat alerts

    Args:
        data: satellite log file

    Returns:
        Smiles
    """
    satellite_threshold_hits = get_threshold_hits(data=data)
    hit_ids = set([s["satelliteId"] for s in satellite_threshold_hits])
    alert_data = set_alert_component(hit_ids, satellite_threshold_hits)
    alert_notif = set_alert_notif(hit_ids, alert_data)

    logging.info("Running Command...")
    logging.info("Found Satellites in Alert: {} Total".format(len(hit_ids)))

    return alert_notif
